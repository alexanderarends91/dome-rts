﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Miner : Building
{
    /// <summary>
    /// The speed at which the miner will gather resources
    /// </summary>
    [Header("Miner Specific stuff")]
    public float minerSpeed;

    /// <summary>
    /// The resources gathered every X minerSpeed
    /// </summary>
    public int resourceGathered;

    /// <summary>
    /// Required to get resources at interval
    /// </summary>
    private float timer;

    void Start()
    {
        timer = minerSpeed;
    }

    void Update()
    {
        if (activated)
        {
            timer -= Time.deltaTime * 1;

            if (timer <= 0)
            {
                ResourceManager.Instance.GainResource(resourceGathered);
                timer = minerSpeed;
            }
        }
    }
}
