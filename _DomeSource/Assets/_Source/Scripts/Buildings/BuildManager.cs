﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The build manager will place buildings
/// Manage all buildings placed
/// Progress the actually building of the buildings
/// Check with the resource manager if the player can build the building request to build
/// </summary>
public class BuildManager : SingletonBehaviour<BuildManager>
{
    /// <summary>
    /// Temporary stuff, will be replaced later
    /// </summary>
    public Building debugBuildingPrefab;

    /// <summary>
    /// The container for all buildings built, for keeping the scene organized
    /// </summary>
    public Transform buildingContainer;

    /// <summary>
    /// The material used to indicate blueprints
    /// </summary>
    public Material bluePrintMaterial;

    /// <summary>
    /// The current blueprint
    /// </summary>
    Building currentBlueprint;

    /// <summary>
    /// Shows a blueprint of the building that we want to build, since it was the one selected
    /// </summary>
    /// <param name="b">The building we are going to show the blueprint for</param>
    /// <param name="pos">The position we will display the blueprint</param>
    internal void ShowBluePrint(Building b, Vector3 pos)
    {
        if (currentBlueprint == null)
        {
            // Instantiate and rename it to blueprint
            GameObject newBlueprint = Instantiate(b.gameObject, pos, Quaternion.identity, buildingContainer);
            newBlueprint.name = "Blueprint";

            // Set the material to a blue print material
            Building bComp = newBlueprint.GetComponent<Building>();
            bComp.SetMaterial(bluePrintMaterial);

            // Set the current blueprint as this one
            currentBlueprint = bComp;
            currentBlueprint.transform.position = pos + new Vector3(0, currentBlueprint.yOffset);
        }
        else
        {
            currentBlueprint.transform.position = pos + new Vector3(0, currentBlueprint.yOffset);
        }
    }

    /// <summary>
    /// Whether or not we are currently showing a blueprint for the building that we want to place
    /// </summary>
    /// <returns></returns>
    internal bool ShowingBlueprint()
    {
        if (currentBlueprint != null)
            return true;
        else
            return false;
    }

    /// <summary>
    /// Clears the current blueprint since an action was done where we canceled building it
    /// </summary>
    internal void ClearBlueprint()
    {
        Destroy(currentBlueprint.gameObject);
        currentBlueprint = null;
    }

    /// <summary>
    /// Builds the actual blueprint at the spot given
    /// </summary>
    internal void Build()
    {
        currentBlueprint.ResetMaterial();
        currentBlueprint.name = "Building";
        currentBlueprint.ActivateBuilding();

        // Reset the blueprint
        currentBlueprint = null;

        // Reset our selection
        SelectionManager.Instance.BuiltBuilding();
    }

}
