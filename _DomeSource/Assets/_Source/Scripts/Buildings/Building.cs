﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A building, every building will share this as a main class
/// </summary>
public class Building : MonoBehaviour
{
    /// <summary>
    /// The max HP and simultaneously the start HP of the building
    /// Ranges from 1 to a 100
    /// </summary>
    [Range(0f, 100f)]
    public float maxHP;

    /// <summary>
    /// The current HP this building has
    /// </summary>
    internal float currentHP;

    /// <summary>
    /// The amount of gold required to build the building
    /// </summary>
    public int resourceCost;

    /// <summary>
    /// The time it takes for this building to build in seconds
    /// </summary>
    public float buildTime;

    /// <summary>
    /// Whether or not this building has been activated or not
    /// </summary>
    internal bool activated;

    /// <summary>
    /// The meshRenderer for this building
    /// </summary>
    private MeshRenderer myRenderer;

    /// <summary>
    /// The offset when we are placing this building
    /// Helps the building not sticking in to stuff
    /// </summary>
    internal float yOffset;

    /// <summary>
    /// The material this Building had at it's start
    /// </summary>
    private Material startMaterial;

    void Awake()
    {
        if (myRenderer == null)
            myRenderer = this.GetComponent<MeshRenderer>();

        startMaterial = myRenderer.material;
        yOffset = this.transform.localScale.y / 2;
    }

    /// <summary>
    /// Ground the building so its not sticking through something
    /// For now this method only works for in general flat surfaces
    /// </summary>
    public void GroundBuilding()
    {
        // Current position of the transform
        Vector3 curPos = this.transform.position;

        this.transform.position = new Vector3(curPos.x, curPos.y + yOffset, curPos.z);
    }

    /// <summary>
    /// Activates the building
    /// </summary>
    internal void ActivateBuilding()
    {
        activated = true;
    }

    /// <summary>
    /// Changes the material of this building
    /// </summary>
    /// <param name="material"></param>
    internal void SetMaterial(Material material)
    {
        myRenderer.material = material;
    }

    /// <summary>
    /// Resets the material to its original state
    /// </summary>
    internal void ResetMaterial()
    {
        myRenderer.material = startMaterial;
    }
}