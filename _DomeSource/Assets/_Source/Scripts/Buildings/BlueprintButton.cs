﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using DG.Tweening;

/// <summary>
/// The buttons in the UI that will trigger building a building once they are done
/// </summary>
public class BlueprintButton : MonoBehaviour, IPointerClickHandler
{
    /// <summary>
    /// The building to use data from
    /// </summary>
    public Building building;

    // References to UI stuff
    [Header("UI")]
    public Image background;
    public Image overlay;
    public Text statusText;

    /// <summary>
    /// Wheter or not this button has been selected
    /// </summary>
    bool selected;
    /// <summary>
    /// Whether or not the building has been selected
    /// </summary>
    bool buildingReady;

    /// <summary>
    /// What should happen on left clicking this button
    /// </summary>
    [Header("Click events")]
    public UnityEvent leftClick;
    /// <summary>
    /// What should happen on right clicking this button
    /// </summary>
    public UnityEvent rightClick;

    /// <summary>
    /// Once this has been selected we will start the process of building the building
    /// </summary>
    public void Selected()
    {
        if (!selected)
        {
            selected = true;

            statusText.gameObject.SetActive(true);
            statusText.text = "BUILDING";

            progressBuilding = StartCoroutine(ProgressBuilding());

            // TODO: Link resources instead of time
            overlay.gameObject.SetActive(true);
            //overlay.DOFillAmount(0f, building.buildTime).SetEase(Ease.Linear).OnComplete(() => BuildingComplete()).SetId("Building_" + this.GetInstanceID());
        }
        else if (selected && buildingReady)
        {
            SelectionManager.Instance.SelectedBlueprint(this);
        }
    }

    int resourcesInvested;
    Coroutine progressBuilding;

    /// <summary>
    /// Progress building this building with resources
    /// </summary>
    IEnumerator ProgressBuilding()
    {
        // Get more resources to invest in this building
        resourcesInvested += ResourceManager.Instance.GetResource(1);

        // Calculate the progress we are at
        float progress = (float)resourcesInvested / building.resourceCost;

        // Set the overlay fill amount
        overlay.fillAmount = 1f - progress;

        yield return null;

        if (progress < 1f)
            progressBuilding = StartCoroutine(ProgressBuilding());
        else
            BuildingComplete();
    }

    /// <summary>
    /// When we are done building
    /// </summary>
    void BuildingComplete()
    {
        statusText.text = "COMPLETE";
        background.color = Color.green;
        buildingReady = true;
        overlay.gameObject.SetActive(false);
        overlay.fillAmount = 1f;
    }

    /// <summary>
    /// The building has been built, we will reset back to our original state
    /// </summary>
    internal void BuildingBuilt()
    {
        overlay.fillAmount = 1f;
        resourcesInvested = 0;
        background.color = Color.white;
        selected = false;
        buildingReady = false;
        statusText.gameObject.SetActive(false);
    }

    /// <summary>
    /// When we canceled building this blueprint
    /// </summary>
    public void Canceled()
    {
        if (selected)
        {
            StopCoroutine(progressBuilding);
            ResourceManager.Instance.GainResource(resourcesInvested);

            resourcesInvested = 0;
            selected = false;
            buildingReady = false;
            background.color = Color.white;

            overlay.gameObject.SetActive(false);
            overlay.fillAmount = 1f;
            statusText.gameObject.SetActive(false);
        }
    }


    /// <summary>
    /// When this button is clicked, check what mousebutton was used and execute the correct method
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            leftClick.Invoke();
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            rightClick.Invoke();
        }
    }
}
