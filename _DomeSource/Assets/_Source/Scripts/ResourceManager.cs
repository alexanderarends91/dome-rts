﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Will keep track of all resources the player will have during the game
/// This includes resources that persist and don't persist between games
/// </summary>
public class ResourceManager : SingletonBehaviour<ResourceManager>
{
    [SerializeField]
    private int totalResources = 10000;

    void Start()
    {
        UIManager.Instance.UpdateResourceLabel(totalResources);
    }

    /// <summary>
    /// Will check if the player has enough Gold to spend
    /// </summary>
    /// <param name="resourcesRequired">The gold required for performing the action</param>
    /// <returns></returns>
    internal bool HasEnoughResources(float resourcesRequired)
    {
        return true;
    }

    /// <summary>
    /// To gain more resources
    /// </summary>
    /// <param name="v"></param>
    internal void GainResource(int v)
    {
        totalResources += v;

        UIManager.Instance.UpdateResourceLabel(totalResources);
    }

    /// <summary>
    /// Checks if we have enough resources and returns the requested amount
    /// </summary>
    /// <param name="v">The value we want</param>
    /// <returns></returns>
    internal int GetResource(int v)
    {
        if (v <= totalResources)
        {
            totalResources -= v;
            UIManager.Instance.UpdateResourceLabel(totalResources);
            return v;
        }
        else
            return 0;
    }
}
