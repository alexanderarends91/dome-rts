﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// This will manage all input that is not UI in the game, such as:
/// Selecting buildings
/// Placing buildings
/// Destroying buildings
/// Moving units
/// etc.
/// </summary>
public class InputManager : SingletonBehaviour<InputManager>
{
    /// <summary>
    /// The layers we are allowed to hit with our input
    /// Mostly used for raycasts with the mouse
    /// </summary>
    public LayerMask layersToHit;

    void Update()
    {
        // Keep casting a ray to detect what we are hitting
        CastRay(Camera.main.ScreenPointToRay(Input.mousePosition));

        if (Input.GetMouseButtonDown(1))
        {
            if (SelectionManager.Instance.GetCurrentSelectedType() == SelectionType.Blueprint)
                BuildManager.Instance.ClearBlueprint();


            SelectionManager.Instance.Deselect();
        }
    }

    /// <summary>
    /// Cast a raycast and checks the collision then will get a component and execute according behaviour
    /// </summary>
    /// <param name="r"></param>
    void CastRay(Ray r)
    {
        RaycastHit rH = new RaycastHit();
        Physics.Raycast(r, out rH, 200f, layersToHit);

        if (rH.collider != null)
        {
            // If we are over the UI then we don't want to override this
            if (EventSystem.current.IsPointerOverGameObject())
                return;

            // The layer that was hit
            int layerHit = rH.transform.gameObject.layer;

            // The point in world space where we hit the collider
            Vector3 hitPos = rH.point;

            // We hit the terrain, if our selected object was a building then we will build here
            if (layerHit == LayerMask.NameToLayer("Terrain") && SelectionManager.Instance.GetCurrentSelectedType() == SelectionType.Blueprint)
            {
                BuildManager.Instance.ShowBluePrint(SelectionManager.Instance.GetCurrentBlueprint(), hitPos);

                // When we press the mouse button, we want to build the building
                if (Input.GetMouseButtonDown(0))
                    BuildManager.Instance.Build();
            }
        }
    }
}
