﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SelectionType
{
    None,
    Building,
    Unit,
    Enemy,
    UIButton,
    Blueprint
}

/// <summary>
/// The selection manager will keep track of the current thing the player has selected
/// This can be anything from a building, enemies, units, UI stuff etc.
/// </summary>
public class SelectionManager : SingletonBehaviour<SelectionManager>
{
    [SerializeField]
    private SelectionType currentSelection;

    private BlueprintButton lastSelectedBlueprintButton;

    /// <summary>
    /// Returns the type of the current selected item
    /// </summary>
    /// <returns></returns>
    internal SelectionType GetCurrentSelectedType()
    {
        // For now we just return blueprint since we are making the building of buildings
        return currentSelection;
    }

    /// <summary>
    /// Deselects whatever we had selected
    /// </summary>
    internal void Deselect()
    {
        currentSelection = SelectionType.None;
    }

    /// <summary>
    /// Deselects the blueprint and lets it now that we have built the building
    /// </summary>
    internal void BuiltBuilding()
    {
        if (currentSelection == SelectionType.Blueprint)
        {
            lastSelectedBlueprintButton.BuildingBuilt();
        }

        currentSelection = SelectionType.None;
    }

    /// <summary>
    /// Selected a blueprint from the menu
    /// </summary>
    public void SelectedBlueprint(BlueprintButton b)
    {
        currentSelection = SelectionType.Blueprint;
        lastSelectedBlueprintButton = b;
    }

    /// <summary>
    /// Returns the building of the current blueprint button
    /// </summary>
    /// <returns></returns>
    internal Building GetCurrentBlueprint()
    {
        if (lastSelectedBlueprintButton != null)
            return lastSelectedBlueprintButton.building;
        else
            return null;
    }
}
