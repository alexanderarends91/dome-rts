﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The UIManager will manage a lot of UI stuff, like animations, updating labels etc.
/// </summary>
public class UIManager : SingletonBehaviour<UIManager>
{
    /// <summary>
    /// The label we use to display the resources we currently have
    /// </summary>
    public Text resourcesLabel;

    /// <summary>
    /// Updates the resource labels in the game
    /// </summary>
    /// <param name="totalResources"></param>
    internal void UpdateResourceLabel(int totalResources)
    {
        resourcesLabel.text = "$ " + totalResources;
    }
}
